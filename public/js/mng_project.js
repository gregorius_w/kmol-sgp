var atdb = require('airtable');
var base = new atdb({ apiKey: 'keym73SOxIZgkBGXQ' }).base('appcvkzJUF2e5J6Sj');

var authpage = function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            userid = user.uid;
            loadData();
            console.log('user-authorized');
            //
        } else {

        }
    })
}


var loadData = function () {
    var locations = {};
    var listloc = "{";
    $('#content-data-table').empty();
    //get locations
    base('location').select({
        view: 'Grid view'
    }).eachPage(function page(records, fetchNextPage) {
        records.forEach(function (record) {
            listloc += '"' + record.getId() + '":"' + record.get('Name') + '",';
        })
        fetchNextPage();
    }, function done(err) {
        locations = JSON.parse(listloc.slice(0, listloc.length - 1) + "}");
        getActivityData(locations);
        if (err) { console.error(err); return; }
    });
}

var getActivityData = function(locations){
    base('Project').select({
        maxRecords: 1000,
        sort: [
            { field: 'Project Name', direction: 'asc' }
        ],
        view: 'Grid view'
    }).eachPage(function page(records, fetchNextPage) {
    records.forEach(function (record) {
            var rid = record.getId();
            var actInfo = $('<tr id="'+ rid + '">' );
            var did = '<td>' + rid + '</td>';
            var dloc = '<td>' + locations[record.get('Lokasi')] + '</td>';
            var dnm = '<td>' + record.get('Project Name') + '</td>';
            var delb = $('<button>').addClass("btn waves-effect waves-light")
                .html('<i class="material-icons">delete</i>')
                .attr('title', 'Hapus Dokumen')
                .click(function () {
                    deleteActivity(rid);
                });
            var edib = $('<button>').addClass("btn waves-effect waves-light modal-trigger")
                .html('<i class="material-icons">edit</i>')
                .attr('data-target', 'modalA')
                .click(function () {
                    loadEditForm(rid);
                });
            var mdb = $('<td>').append(delb);
            actInfo.append(dnm, dloc, did, mdb);
            $('#content-data-table').append(actInfo);
        });
        fetchNextPage();
    }, function done(er) {
        $('#container-table').DataTable();
        if (er) { console.error(er); return; }
    });
}

var loadEditForm = function(rec){
    
}

var removerow = function (rec){
    var table = $('#container-table').DataTable();
    table.row($('#' + rec)).remove();
    table.draw();
    console.log("remove row: " + rec);
}

var deleteActivity = function (rec) {
    swal({
        title: "Konfirmasi hapus dokumen",
        text: "Apakah Anda yakin akan menghapus dokumen ini?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then(res => {
        if (res) {
            base('Activity').destroy(rec, function (err, deletedRecord) {
                if (err) { 
                    console.error(err);
                    Materialize.toast('Proses mengalami gangguan. Silahkan coba lagi setelah beberapa saat.', 5000, 'red');
                    return false; 
                } else {
                    Materialize.toast('Dokumen dihapus', 3000, 'green');
                    console.log('Deleted record: ', deletedRecord.id);
                    removerow(deletedRecord.id);
                    return true;
                }
                
                
            });
        }
    })
}

$(document).ready(function () {
    authpage();
    $('.modal').modal();

});

// Datatable click on select issue fix
$(window).on('load', function () {
    $(".dropdown-content.select-dropdown li").on("click", function () {
        var that = this;
        setTimeout(function () {
            if ($(that).parent().parent().find('.select-dropdown').hasClass('active')) {
                // $(that).parent().removeClass('active');
                $(that).parent().parent().find('.select-dropdown').removeClass('active');
                $(that).parent().hide();
            }
        }, 100);
    });
});