// Initialize Firebase
var config = {
    apiKey: "AIzaSyDZB1LKyO-Q_jWsIL2wHNZl19qJhrAQQbY",
    authDomain: "kmol-sgp.firebaseapp.com",
    databaseURL: "https://kmol-sgp.firebaseio.com",
    projectId: "kmol-sgp",
    storageBucket: "kmol-sgp.appspot.com",
    messagingSenderId: "864704080692"
};
firebase.initializeApp(config);
/*const fdb = firebase.firestore();
const settings = { timestampsInSnapshots: true };
    fdb.settings(settings);*/
const fst = firebase.storage();
const frdb = firebase.database();
var userid;
var userdata;

var login = function (email, pass) {
    firebase.auth().signInWithEmailAndPassword(email, pass).then(function () {
        console.log("user login");
        $("#location-menu").removeClass("hide");
        Materialize.toast('User logged in', 3000, 'blue');
        $("#in_pass").val("");
        $("#err-msg").text("");
    }).catch(function (error) {
        // Handle Errors here.
        console.log('Error ' + error.code + ': ' + error.message);
        $("#in_pass").val("");
        $('.preloader-background').delay(500).fadeOut('slow');
        switch (error.code) {
            case 'auth/invalid-email':
                Materialize.toast('Invalid email-password combination', 3000, 'red');
                break;
            case 'auth/user-disabled':
                Materialize.toast('Your account has been disabled. Please contact Principia team.', 3000, 'red');
                break;
            case 'auth/user-not-found':
                Materialize.toast('No such account. Try to register.', 3000, 'red');
                break;
            case 'auth/wrong-password':
                Materialize.toast('Invalid email-password combination', 3000, 'red');
                break;
            default:
                Materialize.toast(error.message, 3000, 'red');
                break;
        }
    });
}
var logout = function () {
    firebase.auth().signOut().then(function () {
        // Sign-out successful.
        console.log("user logout");
        $("#location-menu").addClass("hide");
        Materialize.toast('User logged out', 3000, 'blue');
    }).catch(function (error) {
        // An error happened.
        console.log('Error ' + error.code + ': ' + error.message);
        $('.preloader-background').delay(500).fadeOut('slow');
        Materialize.toast(error.message, 3000, 'red');
    });
}


var resetPassword = function (email) {
    if (email == null || email == "") {
        Materialize.toast('Please enter email address before reset password.', 5000, 'red');
    } else {
        swal({
            title: "Konfirmasi reset password",
            text: "Apakah Anda yakin akan melakukan reset password?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((res) => {
            if (res) {
                var errtext = "";
                firebase.auth().sendPasswordResetEmail(email).then(() => {
                    Materialize.toast('Email reset password terkirim ke: ' + email, 5000, 'blue');
                }).catch(err => {
                    switch (err.code) {
                        case 'auth/invalid-email':
                            errtext = 'Invalid email address.'
                            break;
                        case 'auth/user-not-found':
                            errtext = 'Email not found. Contact Principia Team for further help.'
                            break;
                        default:
                            console.log(err);
                            errtext = err.message;
                            break;
                    }
                    Materialize.toast(errtext, 5000, 'red');
                });
            }
        });
    }
}

function loadUserInfo(uid) {
    return frdb.ref('/users/' + uid).once('value').then(snap => {
        userdata = snap.val();
        $("#top-right-photo").attr("src", userdata.photo);
        if (userdata.access === "admin") {
            loadAdminMenu();
        } else if (userdata.access === "principia") {
            loadPLLMenu();
        } else {
            //loadUserMenu(userdata.location);
            clearSideMenu();
        }
    })
}

function loadAdminMenu() {
    $("#left-sidebar-admin-content").load("/pages/sidebar_admin.htm");
}

function loadUserMenu(loc) {
    $("#left-sidebar-main-content").load("/pages/sidebar_user_" + loc + ".htm");
}

function loadPLLMenu() {
    $("#left-sidebar-admin-content").load("/pages/sidebar_admin.htm");
}

function clearSideMenu() {
    $("#left-sidebar-main-content").html("");
}

function loadForm(id) {
    $(".preloader-background").fadeIn();
    return frdb.ref('/form/' + id).once('value').then(snap => {
        var email = firebase.auth().currentUser.email;
        var iframe = '<iframe class="airtable-embed" src="' + snap.val() + '&prefill_creator=' + email +
            '" frameborder="0" onmousewheel="" width="100%" style="background: transparent; border: 1px solid #ccc; height: calc(100vh - 74px)"></iframe>';
        $("#content-display").load("pages/form.htm", function (resp, statr, xhr) {
            if (statr == "success") {
                $("#dynamic-form-content").html(iframe);
            }
        });
        $("#content").removeClass("hide");
        $("#location-menu").addClass("hide");
        $('.preloader-background').delay(500).fadeOut('slow');
    });
}

var pageList = {
    "manage_community": "manage_community.html",
    "manage_issue": "manage_issue.html",
    "manage_location": "manage_location.html",
    "manage_project": "manage_project.html",
    "manage_sublocation": "manage_sublocation.html",
    "manage_writer": "manage_writer.html"
}


function loadCustomPage(id, adm) {
    $(".preloader-background").fadeIn();
    var pagelocation = pageList[id];
    var iframe = '<iframe src="' + pagelocation +
        '" frameborder="0" onmousewheel="" width="100%" style="background: transparent; border: 1px solid #ccc; height: calc(100vh - 74px)"></iframe>';
    $("#content-display").load("pages/view.htm", function (resp, statr, xhr) {
        if (statr == "success") {
            if (adm == true) {
                $("#dynamic-view-content").load("pages/admin_action_button.htm", function (rsp, sts, xr) {
                    if (sts == "success") {
                        $("#dynamic-view-content").html(function (i, ortxt) {
                            return iframe + ortxt;
                        });
                    }
                });
            } else {
                $("#dynamic-view-content").html(iframe);
            }
        }
    });
    $("#content").removeClass("hide");
    $("#location-menu").addClass("hide");
    $('.preloader-background').delay(500).fadeOut('slow');
}

function loadView(vid, adm) {
    $(".preloader-background").fadeIn();
    return frdb.ref('/user_pages/' + vid).once('value').then(snap => {
        var vcontrol = "";
        if (adm == true) {
            vcontrol = '&viewControls=on'
        }
        var iframe = '<iframe class="airtable-embed" src="' + snap.val() + vcontrol +
            '" frameborder="0" onmousewheel="" width="100%" style="background: transparent; border: 1px solid #ccc; height: calc(100vh - 74px)"></iframe>';
        $("#content-display").load("pages/view.htm", function (resp, statr, xhr) {
            if (statr == "success") {
                if (adm == true) {
                    $("#dynamic-view-content").load("pages/admin_action_button.htm", function (rsp, sts, xr) {
                        if (sts == "success") {
                            $("#dynamic-view-content").html(function (i, ortxt) {
                                return iframe + ortxt;
                            });
                        }
                    });
                } else {
                    $("#dynamic-view-content").html(iframe);
                }
            }
        });
        $("#content").removeClass("hide");
        $("#location-menu").addClass("hide");
        $('.preloader-background').delay(2000).fadeOut('slow');
    });
}
