var authuser = function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            userid = user.uid;
            loadUserInfo(userid);
            //
            $(".side-nav-content").removeClass("hide");
            $(".auth-user-only").removeClass("hide");            
            $(".login-panel").addClass("hide");
            $("#location-menu").removeClass("hide");
            //$("#content-display").load("pages/dashboard.htm");
            console.log("user authenticated");
            //
            $('.preloader-background').delay(500).fadeOut('slow');
        } else {
            userid = null;
            userdata = null;
            //
            $(".side-nav-content").addClass("hide");
            $(".auth-user-only").addClass("hide");
            $("#content").addClass("hide");
            $(".login-panel").removeClass("hide");
            $("#top-right-photo").attr("src", "img/avatar/avatar-10.png");
            //
            $('.preloader-background').delay(500).fadeOut('slow');
        }
    })
}

$(document).ready(function () {
    authuser();
    $(".button-collapse").sideNav();

    $("#btn-login").click(function () {
        $('.preloader-background').fadeIn();
        login($("#in_user").val(), $("#in_pass").val());
    });

    $("#btn-logout").click(function () {
        $('.preloader-background').fadeIn();
        logout();
    });

    $("#btn-m-logout").click(function () {
        $('.preloader-background').fadeIn();
        logout();
    });

    $("#btn-logout-m").click(function () {
        $('.preloader-background').fadeIn();
        logout();
    });

    $("#reset-password").click(function(){
        resetPassword($("#in_user").val());
    });

    $("#in_pass").keypress(function (event) {
        if (event.which == 13) {
            $('.preloader-background').fadeIn();
            login($("#in_user").val(), $("#in_pass").val());
        }
    });
    $(".edit-btn").click(function(){
        $(".preloader-background").fadeIn();
    });

    function loadPage(page) {
        $("#display-content").load("pages" + page + ".txt");
    }
});

function loadPage(page) {
    $(".preloader-background").fadeIn();
    $("#content-display").load("pages/" + page + ".htm");
    $("#content").removeClass("hide");
    $("#location-menu").addClass("hide");
    //console.log("load page: " + page);
    $('.preloader-background').delay(500).fadeOut('slow');
}

function openLocationMenu(){
    $("#content").addClass("hide");
    $("#location-menu").removeClass("hide");
}

function accessLocation(loc){
    $("#left-sidebar-main-content").load("/pages/sidebar_user_"+loc+".htm");
    $("#content").removeClass("hide");
    $("#location-menu").addClass("hide");
}

