var authpage = function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            userid = user.uid;
            loadUserProfile(userid);
            //
        } else {

        }
    })
}

var loadUserProfile = function (uid) {
    return frdb.ref('/users/' + uid).once('value').then(snap => {
        userdata = snap.val();
        $("#profile-image").attr("src", userdata.photo);
        $("#edit-profile-image").attr("src", userdata.photo);
        $("#profile-display-name").html(userdata.fullname);
        $("#in_fullname").val(userdata.fullname);
        $("#profile-display-title").html(userdata.title);
        $("#in_title").val(userdata.title);
    })
}

// file upload
var stref = firebase.storage().ref();
var fdb = firebase.database();
var file;
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'bmp':
        case 'png':
            //etc
            return true;
    }
    return false;
}
var handleFileSelect = function () {
    if (file != null && isImage(file.name)) {
        // Push to child path.
        // [START oncomplete]
        swal({
            title: "Confirm Upload",
            text: "Upload selected files?",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willUp) => {
           
            if (willUp) {
                var metadata = {
                    'contentType': file.type
                };
                var ext = getExtension(file.name);
                var userid = firebase.auth().currentUser.uid;
                var uploadTask = stref.child('userphoto/' + userid + "." + ext).put(file, metadata);
                $('.preloader-background').fadeIn();
                uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, function (snapshot) {
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                    }
                }, function (error) {
                    console.error('Upload failed:', error);
                    Materialize.toast('Error while uploading file!', 3000, 'red');
                    $('.preloader-background').delay(500).fadeOut('slow');
                }, function () {
                    //then
                    uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                        fdb.ref("users/" + userid).once('value').then(rs => {
                            var dataUpdate = {
                                access: rs.val().access,
                                fullname: rs.val().fullname,
                                location: rs.val().location,
                                photo: downloadURL,
                                title: rs.val().title
                            }
                            fdb.ref("users/" + userid + "/").set(dataUpdate).catch(function (e) {
                                console.error('error registering file:', error);
                                Materialize.toast('Error registering file!', 3000, 'red');
                                $('.preloader-background').delay(500).fadeOut('slow');
                            });
                        });

                        //console.log('File available at', downloadURL);
                        $("#profile-image").attr("src", downloadURL);
                        $("#edit-profile-image").attr("src", downloadURL);
                        $('.preloader-background').delay(500).fadeOut('slow');
                    });
                });

            } else {
                $('.preloader-background').delay(500).fadeOut('slow');
                Materialize.toast('Ipload cancelled.', 3000, 'red');
            }
        });

    } else {
        $('.preloader-background').delay(500).fadeOut('slow');
        Materialize.toast('No file selected or file is not image.', 3000, 'red');
    }
}

var safeUserProfile = function () {
    $('.preloader-background').fadeIn();
    var newfn = $("#in_fullname").val();
    var newtl = $("#in_title").val();
    fdb.ref("users/" + userid).once('value').then(rs => {
        var dataUpdate = {
            access: rs.val().access,
            fullname: newfn,
            location: rs.val().location,
            photo: rs.val().photo,
            title: newtl
        }
        fdb.ref("users/" + userid + "/").set(dataUpdate,
            function () {
                $("#profile-display-name").html(newfn);
                $("#in_fullname").val(newfn);
                $("#profile-display-title").html(newtl);
                $("#in_title").val(newtl);
                $('.preloader-background').delay(500).fadeOut('slow');
                Materialize.toast('Profile Updated', 3000, 'blue');
            }).catch(function (e) {
                console.error('error updating profile:', error);
                Materialize.toast('Error updating profile!', 3000, 'red');
                $('.preloader-background').delay(500).fadeOut('slow');
            });
    });
}


$(document).ready(function () {
    authpage();
    $("#edit-profile").click(function () {
        //tbd
        $("#edit-profile-form").toggleClass("hide");
    });
    $("#btn-save-profile").click(function () {
        //tbd
        safeUserProfile();
    });
    $('#a_fileupload').on('change', function (evt) {
        try {
            evt.stopPropagation();
            evt.preventDefault();
            file = evt.target.files[0];
            $(".file-path").val(file.name);
            console.log('file selected:' + file.name);
        } catch (err) {
            console.error(err);
        }
    });

    $('#uploadBtn').click(function () {
        handleFileSelect();
    });
});